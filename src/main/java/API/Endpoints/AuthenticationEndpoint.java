package API.Endpoints;

import API.Service.DefaultGoogleService;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Produces(MediaType.APPLICATION_JSON)
@Path("/auth")
public class AuthenticationEndpoint {

    @Inject
    private DefaultGoogleService googleService;

    @POST
    @Path("/google")
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.TEXT_PLAIN)
    public Response postGoogle(String token){
        System.out.println("Token: " + token);
        String jwtToken = "";

        try{
            jwtToken = googleService.postGoogle(token);
        } catch (Exception e){
            e.printStackTrace();
        }
        if(jwtToken.equals("")){
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }
        return Response.ok((jwtToken)).build();
    }
}
