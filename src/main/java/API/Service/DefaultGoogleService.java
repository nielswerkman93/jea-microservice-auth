package API.Service;

import JWT.JwtManager;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonObject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.StringReader;

@ApplicationScoped
public class DefaultGoogleService {

    @Inject
    private JwtManager jwtManager;

    public String postGoogle(String token) {
        System.out.println("PostGoogle Method");
        Client client = ClientBuilder.newClient();
        WebTarget base = client.target("https://www.googleapis.com/oauth2/v1/userinfo?alt=json");
        WebTarget url = base.queryParam("access_token", token);
        Response response = url.request(MediaType.APPLICATION_JSON).get();
        System.out.println("Response logging status: " + response.getStatus());
        if(response.getStatus() == 200){
            Response userResponse = checkUser(response);
            System.out.println("userResponse code: " + userResponse.getStatus());
            if (userResponse.getStatus() == 200) {
                JsonObject o = Json.createReader(new StringReader(userResponse.readEntity(String.class))).readObject();
                String email = o.get("userName").toString().replace("\"", "");
                int id = Integer.parseInt(o.get("id").toString().replace("\"", ""));

                String jwtToken = null;
                try {
                    jwtToken = jwtManager.createJwt(email, id, "DEFAULT");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return jwtToken;
            }
        }
        return "";
    }

    private Response checkUser(Response response){
        JsonObject o = Json.createReader(new StringReader(response.readEntity(String.class))).readObject();
        String email = o.get("email").toString().replace("\"", "");

        System.out.println("Email: " + email);

        Client client = ClientBuilder.newClient();
        WebTarget url = client.target("http://192.168.178.87:9100/api/user/" + email);
        Response userResponse = url.request(MediaType.APPLICATION_JSON).get();
        return  userResponse;
    }
}